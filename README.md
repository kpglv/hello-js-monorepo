# hello-js-monorepo

Exploring possibilities and problems that may be encountered with
a JS codebase placed in a monorepository.

This monorepo follows the approach to code organization introduced by
[Polylith](https://polylith.gitbook.io/polylith/). It can contain
several kinds of reusable building blocks (components, bases, projects)
from which solutions to specific problems are composed.

Each building block is an NPM package that is also a [NPM workspace](https://docs.npmjs.com/cli/v7/using-npm/workspaces),
so it can be published to some package registry as any other NPM package, but
can also be added to dependencies and referenced by other building blocks
locally, at the source code level, without publishing it first; their
external dependencies can be kept together at the monorepo root level,
deduplicated and shared. See `lerna.json` and `package.json#/workspaces`
for configuration options.

For examples, see

-   the `components/consumer` building block, that depends on the
    `components/provider` building block locally, at the source code level, and
    thus does not require it to be published
-   the `guides/hello-npm` project in the `js` repo (outside of this monorepo),
    that depends on both `components/provider` and `components/consumer`
    building blocks externally, at the artifact level, and thus requires them
    to be published

## Getting started

This monorepo uses [Lerna](https://lerna.js.org/) to run repository-wide tasks
--- package scripts of multiple packages (NPM workspaces), see examples below.

Alternatively, npm can run npm builtin tasks (like `version` or `publish`) or
package scripts on selected packages (NPM workspaces) or all of them at once:

```
npm version patch --workspaces
npm run build -w @example/provider
npm publish -ws
```

### Build

All components

```
npx lerna run build
```

A specific component

```
npx lerna run build --scope=@example/consumer
```

### Test (TODO: add tests)

For all components

```
npx lerna run test
```

For a specific component

```
npx lerna run test --scope=@example/consumer
```

### Run

All components

```
npx lerna run start
```

A specific component

```
npx lerna run start --scope=@example/consumer
```

### Publish and install

Prepare and source the `.env` file (see `example.env`) to set/export
required environment variables (including package registry's auth token).

TODO: make it work on Windows (cmd, Powershell)

```
source .env
```

All components

```
npm publish --workspaces
```

Specific components: in the component's folder

```
npm publish
```

Alternatively, you can set the required auth token environment variable
directly on the command-line

```
NPM_TOKEN=<token> npm publish
```

See `.npmrc` file and `package.json` files in specific building blocks
for details on configuration related to publishing and using the published
packages.
