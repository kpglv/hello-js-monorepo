# provider

An example component that provides for (exports values to, is a dependency of)
other components, and can be consumed both locally (being unpublished, on the
source code level) and externally (being published, on the artifact level).

All building blocks that are configured as NPM workspaces can depend on
other such building blocks locally, any other code can depend on them externally.

## Getting Started

### Build the code

```
npm run build
```

### Run unit tests

```
npm run test
```

### Run the built code

```
npm run start
```

### Publish the package

Prepare and source the `.env` file (see `example.env` in
the repo's root directory) to set required environment variables
(including package registry's auth token).

```
source ../../.env
```

then publish the package

```
npm publish
```

Alternatively, you can set the environment variable with the auth token
directly on the command-line

```
NPM_TOKEN=<token> npm publish
```
