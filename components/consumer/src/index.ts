import { add } from '@example/provider/src';

/**
 * Prints the sum of the given two numbers to console.
 */
export const printSum = (x: number, y: number) =>
    console.log('The sum of %o and %o is %o', x, y, add(x, y));

console.log('loaded "consumer"');
