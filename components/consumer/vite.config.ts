import { resolve } from 'path';
import { defineConfig } from 'vite';

export default defineConfig({
    build: {
        rollupOptions: {
            input: 'src/index.ts',
        },
        lib: {
            entry: resolve(__dirname, 'src/index.ts'),
            name: 'consumer',
            fileName: 'consumer',
        },
    },
});
