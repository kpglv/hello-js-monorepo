# Publishing/using NPM packages with Gitlab package registry

-   Package registry https://docs.gitlab.com/ee/user/packages/npm_registry
-   npm packages in the package registry
    https://docs.gitlab.com/ee/user/packages/npm_registry/
-   Working with the NPM GitLab Registry
    https://handbook.gitlab.com/handbook/support/workflows/how-to-create-npm-package/
