# tasks

## Tasks

### Create the monorepo

Add monorepo-wide dependencies and configuration files:

-   `npm install -D lerna typescript vitest`

-   `tsc --init`

-   `lerna.json`

Add per-component configuration files:

-   For all components:

    -   `vite.config.ts`

-   For the `consumer` component:

    -   `npm install @example/provider`
